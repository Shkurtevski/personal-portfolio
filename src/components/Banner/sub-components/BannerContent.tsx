import React from "react";
import Tagline from "./Tagline";
import TextRotator from "./TextRotator";
import { bannerText } from "../../../data/data";

interface BannerContentProps {
  text: string;
  additionalText: string;
}

const BannerContent = ({ text, additionalText }: BannerContentProps) => (
  <React.Fragment>
    <Tagline text={additionalText} />
    <TextRotator text={text} rotationText={bannerText} />
    <p>
      My journey as a Software Developer has been fueled by a relentless pursuit
      of knowledge. I'm a firm believer in lifelong learning, constantly
      exploring new technologies and design trends to stay at the forefront of
      the industry.
    </p>
  </React.Fragment>
);

export default BannerContent;
