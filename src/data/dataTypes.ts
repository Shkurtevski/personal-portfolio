export interface Project {
  title: string;
  description: string;
  descriptionTwo?: string;
  imgUrl: string;
  btnText1: string;
  btnText2?: string;
  website: string;
  github?: string;
}
